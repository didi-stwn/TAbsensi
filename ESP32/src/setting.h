#include <Ethernet.h>
//#include <EthernetLarge.h>
#include <SPI.h>
#include <Adafruit_Fingerprint.h>
#include <HTTPClient.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <RTClib.h>

#define buzzer 15
#define indikator_baterai 34
#define nofinger 999
#define notfound 998

//Setting Ethernet
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 0, 177);
IPAddress myDns(192, 168, 0, 1);
EthernetClient client;
byte server[] = {192, 168, 1, 26}; //URL API server

//Setting RTC
RTC_DS3231 rtc;
Adafruit_Fingerprint finger = Adafruit_Fingerprint(&Serial2);

//Setting Wifi
//Wifi Router PAU
//const char *ssid = "Solid State Room";
//const char *password = "project_fiveG";

////Wifi Router Didi
//const char *ssid = "TA026";
//const char *password = "limapuluhribu";

//Wifi Kosan Didi
const char *ssid = "Wifi-Kita";
const char *password = "limapuluhribu";

//Setting URL API
String address_url = "https://192.168.1.26:3000";
//String address_url_pengguna_read_matkul = "/pengguna/readmatkul";
//String address_url_pengguna_read = "/pengguna/readpengguna";
//String address_url_fingerprint_template_create = "/fingerprint/create";
//String address_url_log_create = "/log/create";
//String address_url_update_device = "/ruangan/update_last_seen";
//String address_url_update_device_finish = "/ruangan/update_device";


//Setting Device
String kode_device = "260001";
String nim[300];
String nama[300];
String kode_ruangan = "\0";
String kode_matakuliah;
String next_kode_matakuliah;
String kelas;
String token = "token";
bool getFinger = false; //true saat ada kelas
bool Downloaded_Data = false; //true saat berhasil download data dari server
bool update_data_from_ui; //true saat ada jam kosong selama 2 jam atau lebih
short counter_download = 0; //sebagai indikator berhasil download atau tidak, counter_download akan bertambah pada fungsi getDataPengguna
byte counter_dosen = 0;
byte counter_asisten = 0;

//kelas tambahan
String matkul_tambahan;
String durasi_tambahan;
String kelas_tambahan;

//setting RTC
char daysOfTheWeek[7][12] = {"7", "1", "2", "3", "4", "5", "6"};

DynamicJsonDocument httpGetRequest(uint16_t size_response, byte api_server[], String api_host, String api_url) {
  String api_url_full = api_host + api_url;
  char endOfHeaders[] = "\r\n\r\n";
  DynamicJsonDocument output(size_response);

  if (Ethernet.linkStatus() == LinkON) {
    Serial.println("Request with ethernet!");
    Ethernet.begin(mac);
    if (client.connect(api_server, 3000)) {
      Serial.println("Connect Backend");
      client.println("GET " + api_url + " HTTP/1.1");
      client.println("Host: " + api_host);
      client.println("x-access-token: " + token);
      client.println("User-Agent: arduino-ethernet");
      client.println("Connection: close");
      client.println();
      client.find(endOfHeaders);
      deserializeJson(output, client);
      //      WiFi.disconnect();
    }
  }
  else if (WiFi.status() == WL_CONNECTED) {
    Serial.println("Request with Wifi!");
    HTTPClient http;
    http.begin(api_url_full.c_str());
    http.addHeader("x-access-token", token);
    int httpCode = http.GET();
    if ((httpCode > 0) && (httpCode < 300)) {
      deserializeJson(output, http.getStream());
    }
    http.end();
  }
  else {
    Serial.println("Connecting Wifi");
    WiFi.begin(ssid, password);
  }
  Serial.println(ESP.getFreeHeap());
  return output;
}

DynamicJsonDocument httpPostRequest(uint16_t size_response, byte api_server[], String api_host, String api_url, String api_body) {
  String api_url_full = api_host + api_url;
  char endOfHeaders[] = "\r\n\r\n";
  DynamicJsonDocument output(size_response);

  if (Ethernet.linkStatus() == LinkON) {
    Serial.println("Request with ethernet!");
    Ethernet.begin(mac);
    if (client.connect(api_server, 3000)) {
      Serial.println("Connect Backend");
      client.println("POST " + api_url + " HTTP/1.1");
      client.println("Host: " + api_host);
      client.println("x-access-token: " + token);
      client.println("Content-Type: application/x-www-form-urlencoded");
      client.println("User-Agent: arduino-ethernet");
      client.println("Connection: close");
      client.print("Content-Length:");
      client.println(api_body.length());
      client.println();
      client.print(api_body);
      client.println();
      client.find(endOfHeaders);
      deserializeJson(output, client);
      //      WiFi.disconnect();
    }
  }
  else if (WiFi.status() == WL_CONNECTED) {
    Serial.println("Request with Wifi!");
    HTTPClient http;
    http.begin(api_url_full.c_str());
    http.addHeader("x-access-token", token);
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");
    int httpCode = http.POST(api_body);
    if ((httpCode > 0) && (httpCode < 300)) {
      deserializeJson(output, http.getStream());
    }
    http.end();
  }
  else {
    Serial.println("Connecting Wifi");
    WiFi.begin(ssid, password);
  }
  return output;
}

void speedBuzzer(short waktu, byte times) {
  for (byte i = 1; i <= times; i++) {
    digitalWrite(buzzer, HIGH);
    delay(waktu);
    digitalWrite(buzzer, LOW);
    delay(waktu);
  }
}

DateTime getTime() {
  byte bulan, tanggal, jam, menit, detik;
  DateTime now;
  while (1) {
    now = rtc.now();
    bulan = now.month();
    tanggal = now.day();
    jam = now.hour();
    menit = now.minute();
    detik = now.second();
    if ((bulan < 13) && (tanggal < 32) && (jam < 25) && (menit < 61) && (detik < 61)) {
      break;
    }
  }
  //  Serial.println("" + String(bulan) + String(tanggal) + String(jam) + String(menit) + String(detik));
  return now;
}
