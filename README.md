# INSTALASI PROJECT

clone project dengan cara berikut ini: (dengan terminal pada ubuntu)

pilih direktori untuk tempat penyimpanan file
lalu klik kanan dan buka terminal dengan klik "Open in Terminal"
lalu ketik 

`git clone https://gitlab.com/didi-stwn/TAbsensi.git`

lalu terminal akan secara otomatis mendownload file yang ada di GitLab
setelah file selesai diClone, masuk ke direktorinya dengan cara

`cd ta`

didalam folder ta ini akan ada 3 folder: folder untuk arduino, folder frontend, dan folder backend

Dalam project ini hanya membutuhkan 1 software yaitu Visual Studio Code yang dapat di download dari link berikut: https://code.visualstudio.com/

# Front End

masuk direktori frontend lalu ketik 

`sudo npm install` 

untuk menginstall node_modules, tunggu beberapa saat hingga proses selesai.

lalu ketik 

`npm start` 

untuk memulai memasang pada server

jika ditemukan error sebagai berikut:
`module not found: cant resolve ''xlxs` ...`

buka folder tempat error berada `./node_modules/tempa-xlsx/`

kemudian hapus isi file `ods.js` atau buat file document baru bernama `ods.js` sehingga isi dari `ods.js` 

kemudian balik ke direktori 

`./Frontend`

lalu ketik 

`npm start`

lalu tunggu beberapa saat dan buka browser lalu buka buka localhost:1234 atau ip berdasarkan PCnya



## Cara Build

jika perintah `npm start ` dapat berjalan dengan baik, maka langkah berikutnya adalah build project yang kemudian akan dipasang di webservernya

pertama, buka direktori tempat clone berada lalu ketik

`sudo npm run build`

tunggu hingga proses selesai. setelah selesai akan terbentuk folder baru bernama `build`

file build sudah selesai dibuat, kemudian langkah berikutnya disesuaikan dengan web browser yang diinginkan seperti nginx, apache dan sebagainya

untuk mencoba menjalankan dapat melakukan command : `npm run deploy`

### untuk nginx sourcenya sebagai berikut 

buka direktori `/etc/nginx/sites-available` lalu buat file baru (namanya bebas) dan isinya sebagai berikut 

```
server {
  listen 80;
  root /home/ta/frontend/build     //tempat direktori build berada
  server_name [domain.com];
  index index.html index.htm;
  location / {
    try_files $uri /index.html
  }
}
```

lalu link ke sites-enable dengan perintah

`sudo ln -s /etc/nginx/sites-available/[namafile] /etc/nginx/sites-enabled/`

lalu restart nginx dengan perintah 

`sudo service nginx restart`



## Dokumentasi Penggunaan React JS

Berikut beberapa hal yang diperlukan untuk dapat mengcode React JS

1. Install Node JS dengan perintah `sudo apt-get update` lalu `sudo apt-get install nodejs`

2. Lalu cek versi nodejs dan npmnya dengan perintah `node -v` dan `npm -v`

3. Jika npm dan node sudah terinstall  maka langsung dapat membuat app reactnya, sebelumnya untuk mengupdate npm dapat dilakukan perintah `npm install npm@latest -g`

4. Berikutnya membuat file baru react

5. Pilih direktori buat tempat penyimpanan folder app react

6. kemudian jalankan perintah `npm create-react-app [nama folder]`

7. setelah proses instalasi selesai masuk ke dalam folder reactnya dengan perintah `cd [nama folder]`

8. lalu ketik perintah `npm start` untuk memulai programnya lalu akan muncul secara otomatis di browser dengan link `localhost:3000`

9. untuk membuildnya dapat dilakukan perintah `sudo npm run build`

10. untuk mengubah port dapat dilakukan pengeditan pada file package.json pada bagian scriptnya dengan menambahkan PORT=[port yang diinginkan] pada start, berikut untuk port 8000

```
"scripts": {
    "start": "PORT=8000 react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test --env=jsdom",
    "eject": "react-scripts eject"
  },
  ```


ada 3 penting file dalam react yaitu index.html, App.js, dan index.js

jadi index.html itu yang ditampilkan pada browser, isinya dari App.js yang dihubungkan dengan index.js, jadi jika dideploy pada browser, kode pada App.js tidak akan terlihat. jadi dia ada div dengan id root, dimana id root ini akan di link ke app.js dengan index.js

maksud ReactDOM.render yaitu menjalankan App dari app.js yang dimasukan ke id root yang ada di index.htmlnya


salah 1 keunggulan react yaitu dapat terdiri dari beberapa komponent, pada kasus ini saya buat 3 komponent yang dapat dibagi menjadi 3 bagian yaitu header, content, dan sidebar. jadi ini memudahkan saya dalam melakukan coding dimana saya hanya perlu edit di bagian contentnya, cara buatnya yaitu memanggil menu, header, content pada App.js

dimana pada file saya komponent 3 itu (menu, header, dan content) ada didalam file ./src/component

dan isi contentnya ada didalam ./src/component/content

untuk route dan transition routenya saya menggunakan module dari react-router-dom, dapat dicari pada link berikut: https://www.npmjs.com/package/react-router-dom dan transitionnya pada link: https://www.npmjs.com/package/react-router-transition.

Didalam kodenya saya menggunakan lifecycle pada react yaitu Componentdidmount yang berfungsi untuk menjalankan perintah yang ada didalamnya saat pertamakali komponent dibuka, kemudian untuk algoritma saya masukan didalam render dan direturn saya tampilkan hasilnya dalam bahasa html.




# Back End

## Table of Contents
1. [Prerequisites](#Prerequisites)
2. [Installation](#Installation)
    - [Database PostgreSQL](#Database-PostgreSQL)
    - [Server Node JS](#Server-Node-JS)
3. [Run Server](#Run-Server)
4. [API Request](#API-Request)
5. [Instalasi Express JS](#Instalasi-Express-JS)

## Prerequisites
- PostgreSQL
```sh
$ sudo apt install postgresql
```
- Node.JS and NPM (NodeJS Package Manager)
```sh
$ sudo apt install nodejs
$ sudo apt install npm
```
## Installation
### Database PostgreSQL
Database dibuat menggunakan 2 tabel dengan format berikut:
- Tabel *log*

|column|Data Type|Modifiers|Description|
|:---:|:----:|:----:|:---:|
|nim|varchar(30)|not null|primary key|
|nama|varchar(30)|not null||
|waktu|timestamptz|not null||
|kodematkul|varchar(30)|not null||
|koderuangan|varchar(30)|not null||

- Tabel *pengguna*

|column|Data Type|Modifiers|Description|
|:---:|:----:|:----:|:---:|
|fakultas|varchar(30)|not null||
|jurusan|varchar(30)|not null||
|nim|varchar(30)|not null|primary key|
|nama|varchar(30)|not null||
|finger1|varchar(50)|not null||
|finger2|varchar(50)|not null||

Penjelasan tentang tipe data di postgreSQL dapat dilihat di http://www.postgresqltutorial.com/postgresql-data-types/

**Langkah pembuatan database dan tabel**
1. Buat database baru
```sh
$ sudo -u postgres psql
postgres=# create database <nama database>;
```
2. Buat user baru dan berikan akses penuh ke database baru yang sudah dibuat
```sh
postgres=# create user <nama user (diharap user laptop)> with encrypted password '<user password (diharap password laptop)>';
postgres=# grant all privileges on database <nama database> to <nama user>;
postgres=# \q
```
3. Masuk ke database yang telah dibuat
>Untuk log in dengan ident based authentication dibutuhkan nama linux user yang sama dengan nama user pada postgresql, jika berbeda dapat ditambahkan dengan cara `sudo adduser <nama user>`

```sh
$ sudo -i -u <nama user>
$ psql <nama database>
```
>kemudian akan muncul `<nama database>=#` pada terminal
4. Buat tabel baru dengan nama card dan terminal menggunakan format seperti [diatas](#Database-postgreSQL)
```sql
create table log(
    nim varchar(30) primary key,
    nama varchar(30) not null,
    waktu timestamptz not null,
    kodematkul varchar(30) not null,
    koderuangan varchar(30) not null
);

create table pengguna(
    fakultas varchar(30) not null,
    jurusan varchar(30) not null,
    nim varchar(30) primary key,
    nama varchar(30) not null,
    finger1 varchar(50) not null,
    finger2 varchar(50) not null
);
```

### Server Node JS
1. Masuk ke directory repository `backend`, kemudian masukkan perintah:
```sh
$ sudo npm install
```
2. Cek settingan database pada file config.js


## Run Server
Nyalakan server dengan memasukkan perintah `npm start` pada *root directory* repository ini

## API Request
Server API ini memiliki 4 fungsi utama untuk modifikasi database, fungsi-fungsi tersebut adalah *Read*, *Create*, *Edit*, dan *Delete*. Serta semua autentikasi dilakukan dengan cara memberikan header pada setiap request dengan format  'x-access-token:{token}'. untuk lebih jelasnya dapat dilihat pada file API.sh


### Instalasi Express JS
pilih direktori tempat aplikasi express akan diinstall atau buat direktori baru dengan cara:
```sh
$ mkdir myapp
$ cd myapp
```

kemudian, install express js dengan cara: 
```sh
$ npx express-generator
```

kemudian, install node module dengan cara: 
```sh
$ npm install
```

untuk memulai aplikasinya dapat dilakukan dengan cara:
```sh
$ npm start
```



# ESP32
ESP32 ini dijalankan dengan bantuan framework tambahan dari Visual Studio Code yaitu platform.io, berikut tata cara penggunaanya:
1. install platform.io, klik extentions > ketik 'platform io' > install.
2. open folder `./ESP32` pada VSCode.
3. hubungkan board ESP32 tipe WEMOS LOLIN32.
4. klik menu dari platform.io yang ada di sebelah kiri, lalu klik upload.
5. tunggu hingga proses upload selesai.



### Author:

**Didi Setiawan**

Email: didi.stwn.16@gmail.com

Phone Number: +62 859 2009 0113