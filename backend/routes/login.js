const express = require('express');
const router = express.Router();
const path = require('path');
const db = require(path.join(__dirname,'..','controllers','queries'))
const auth = require(path.join(__dirname,'..','controllers','auth'))


router.post('/get_user', auth.checkToken, db.getUser);
router.post('/register', auth.checkToken, db.registerUser);
router.post('/forgot_password', db.updatePassword);
router.post('/delete_user', auth.checkToken, db.deleteUser);
router.post('/login', db.login);

// router.get('/', (req, res, next) => {
//     res.render('login')
// })
  
// router.post('/', (req, res, next) => {
//     var username = req.body.username;
//     var password = req.body.password;
//     var getusername = config.username_login
//     var getpassword = config.password_login

//     if (username && password) {
//         if (username === getusername && password === getpassword) {
//             var token = jwt.sign({username: username},
//                         config.secret,
//                         { expiresIn: '900000'});
//             console.log('New token auth')
//             // return the JWT token for the future API calls
//             res.json({
                // status: 1,
                // message: 'Authentication successful!',
                // date: new Date().toLocaleString(),
                // token: token
//             });
//         } else {
//             console.log('Login failed')
//             res.json({
//                 status: 0,
//                 message: 'Incorrect username or password'
//             });
//         }
//     } 
//     else {
//         console.log('Login failed')
//         res.send(400).json({
//             status: 0,
//             message: 'Authentication failed! Please check the request'
//         });
//     }
// });

module.exports = router;
