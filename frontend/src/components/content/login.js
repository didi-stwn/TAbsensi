import React from 'react';
import logo from './img/logoitb.png';
import get from './config';
import { Redirect } from "react-router-dom";
import hash from 'object-hash';

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            pesan: '',
            gagal: false,
            isLogin: false,
            showlogin: 'show',
            showforgot: 'hide',

            //edit
            usernameu: '',
            password1u: '',
            password2u: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSubmitEdit = this.handleSubmitEdit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();
        const { username, password } = this.state
        fetch(get.login, {
            method: 'post',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                username: username,
                password: hash(password)
            })
        })
            .then(response => response.json())
            .then(response => {
                if (response.status === 0) {
                    this.setState({ gagal: true })
                    this.setState({ pesan: '*username or password is incorrect' })
                    setTimeout(() => { this.setState({ gagal: false }) }, 4000);
                }
                else {
                    sessionStorage.setItem("name", response.token)
                    sessionStorage.setItem("index", response.index)
                    this.setState({ isLogin: true })
                }
            })
            .catch(error => {
                this.setState({ gagal: true })
                this.setState({ pesan: '*Connection loss' })
                setTimeout(() => { this.setState({ gagal: false }) }, 4000);
            })
    }

    handleSubmitEdit(e) {
        e.preventDefault();
        const { usernameu, password1u, password2u } = this.state;
        if (password1u === password2u) {
            fetch(get.forgotPassword, {
                method: 'post',
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify({
                    username: usernameu,
                    password: hash(password1u),
                })
            })
                .then(response => response.json())
                .then(response => {
                    if (response.status === 0) {
                        this.setState({ gagal: true })
                        this.setState({ pesan: '*username or password is incorrect' })
                        setTimeout(() => { this.setState({ gagal: false }) }, 4000);
                    }
                    else {
                        this.setState({ gagal: true })
                        this.setState({ pesan: 'Password Changed' })
                        setTimeout(() => { this.setState({ gagal: false }) }, 2000);
                        setTimeout(() => { this.setState({ showlogin: 'show', showforgot: 'hide' }) }, 1000);
                    }
                })
                .catch(error => {
                    this.setState({ gagal: true })
                    this.setState({ pesan: '*Connection loss' })
                    setTimeout(() => { this.setState({ gagal: false }) }, 4000);
                })
        }
        else {
            this.setState({ databenar: false })
            this.setState({ datasalah: true })
            this.setState({ pesan: 'Password is incorrect' })
        }
    }
    showLogin() {
        this.setState({
            showlogin: 'show',
            showforgot: 'hide'
        })
    }
    showForgot() {
        this.setState({
            showlogin: 'hide',
            showforgot: 'show'
        })
    }
    render() {
        const { isLogin } = this.state
        document.title = "Login"
        const { gagal } = this.state;
        return (
            <div className="login">
                <div className="backgroundlogin"></div>
                <div className="backgroundloginn">
                    <div className="paddingtop100px"></div>
                    <div id={this.state.showlogin} className="backgroundloginnn">
                        <div className="imglogin" >
                            <img src={logo} alt="User"></img>
                        </div >
                        <div className="formform">
                            <form onSubmit={this.handleSubmit}>
                                <label style={{fontWeight:'bold'}} className="loginnim">Username</label> <br></br>
                                <input className="inputnim" name="username" placeholder="Input Username ..." onChange={this.handleChange} type="text" required />
                                <br></br>
                                <label style={{fontWeight:'bold'}} className="loginpass">Password</label> <br></br>
                                <input className="inputnim" name="password" placeholder="Input Password ..." onChange={this.handleChange} type="password" required />
                                <br></br>
                                {
                                    gagal &&
                                    <p className="gagallogin">{this.state.pesan}</p>
                                }
                                <button className="submitform" type="submit">
                                    <span>
                                        <span>
                                            Login&nbsp;
                                        </span>
                                    </span>
                                </button>
                            </form>
                        </div>
                        <div className="texttengah" style={{ paddingBottom: '20px' }}>
                            <button onClick={() => this.showForgot()} className="buttonlikea" style={{ color: 'white', borderBottom: 'solid 1px white', fontSize: '14px' }}>Forgot Password?</button>
                        </div>
                    </div>
                    <div id={this.state.showforgot} className="backgroundloginnn">
                        <div className="imglogin" >
                            <h4 style={{ color: 'white', paddingTop: '12px' }}>Reset Password</h4>
                        </div >
                        <div className="paddingtop30px"></div>
                        <div className="formform">
                            <form onSubmit={this.handleSubmitEdit}>
                                <label style={{fontWeight:'bold'}} className="loginnim">Username</label> <br></br>
                                <input className="inputnim" name="usernameu" placeholder="Input Username ..." onChange={this.handleChange} type="text" required />
                                <br></br>
                                <label style={{fontWeight:'bold'}} className="loginpass">Password</label> <br></br>
                                <input className="inputnim" name="password1u" placeholder="Input Password ..." onChange={this.handleChange} type="password" required />
                                <br></br>
                                <label style={{fontWeight:'bold'}} className="loginpass">Retry Password</label> <br></br>
                                <input className="inputnim" name="password2u" placeholder="Retry Password ..." onChange={this.handleChange} type="password" required />
                                <br></br>
                                {
                                    gagal &&
                                    <p className="gagallogin">{this.state.pesan}</p>
                                }
                                <button className="submitform" type="submit">
                                    <span>
                                        <span>
                                            Reset Password&nbsp;
                                        </span>
                                    </span>
                                </button>
                            </form>
                        </div>
                        <div className="texttengah" style={{ paddingBottom: '20px' }}>
                            <button onClick={() => this.showLogin()} className="buttonlikea" style={{ color: 'white', borderBottom: 'solid 1px white', fontSize: '14px' }}>Login</button>
                        </div>
                    </div>
                    <div className="paddingtop100px"></div>
                    {
                        isLogin &&
                        <Redirect to="/" />
                    }
                </div>
            </div >
        )
    }
} 