#include "fingerprint.h"


void fsm_kelastambahan(uint8_t *input, uint8_t *state) {
  Serial.println("Kelas tambaham mode");
  byte start_time, limit_time, max_durasi;
  bool checkdurasi = false;
  while (1) {
    if (*state == 2)
    {
      lcd.setCursor(0, 0);
      lcd.print("---- Add Class  ----");
      lcd.setCursor(0, 1);
      lcd.print("Kode mata kuliah:   ");
      waitForKeypad(&matkul_tambahan, input);
      Serial.println(matkul_tambahan);
      if (no_key_10 == true) {
        lcd.clear();
        break;
      }
      else {
        if (*input == 3) {
          *state = 3;
        }
        else if (*input == 0) {
          *state = 0;
          lcd.clear();
          break;
        }
        else if (*input == 1) {
          *state = 1;
          lcd.clear();
          break;
        }
        else {
          *state = 2;
        }
      }
    }
    else if (*state == 3)
    {
      lcd.setCursor(0, 1);
      lcd.print("Kode kelas:         ");
      waitForKeypad(&kelas_tambahan, input);
      Serial.println(kelas_tambahan);
      if (no_key_10 == true) {
        lcd.clear();
        break;
      }
      else {
        if (*input == 3) {
          *state = 4;
        }
        else if (*input == 0) {
          *state = 0;
          lcd.clear();
          break;
        }
        else if (*input == 1) {
          *state = 1;
          lcd.clear();
          break;
        }
        else if (*input == 2) {
          *state = 2;
          lcd.clear();
        }
        else {
          *state = 3;
        }
      }
    }
    else if (*state == 4)
    {
      DateTime now = getTime();
      if (now.minute() > 40) {
        max_durasi = counter_next_kelas - now.hour() - 1;
      }
      else {
        max_durasi = counter_next_kelas - now.hour();
      }

      lcd.setCursor(0, 1);
      lcd.print("Durasi Kelas:       ");
      lcd.setCursor(0, 3);
      lcd.print(" Durasi Max: " + String(max_durasi) + " Jam  ");

      waitForKeypad(&durasi_tambahan, input);
      Serial.println(durasi_tambahan);

      if (atoi(durasi_tambahan.c_str()) > max_durasi) {
        checkdurasi = false;
      }
      else {
        checkdurasi = true;
      }

      if (no_key_10 == true) {
        lcd.clear();
        break;
      }
      else {
        if ((*input == 3) && (checkdurasi)) {
          *state = 5;
        }
        else if ((*input == 3) && (checkdurasi == false)) {
          lcd.setCursor(0, 1);
          lcd.print(" Input durasi salah ");
          delay(2000);
          *state = 4;
        }
        else if (*input == 0) {
          *state = 0;
          lcd.clear();
          break;
        }
        else if (*input == 1) {
          *state = 1;
          lcd.clear();
          break;
        }
        else if (*input == 2) {
          *state = 2;
          lcd.clear();
        }
        else {
          *state = 4;
        }
      }
    }
    else if (*state == 5)
    {
      lcd.setCursor(0, 1);
      lcd.print("Kode Matkul : " + matkul_tambahan);
      lcd.setCursor(0, 2);
      lcd.print("Kode Kelas  : " + kelas_tambahan + "   ");
      lcd.setCursor(0, 3);
      lcd.print("Durasi Kelas: " + durasi_tambahan + "     ");

      start_time = start_time_second();
      limit_time = start_time + 10;
      char keypressed = '0';
      byte done = 0;
      while (1) {
        keypressed = myKeypad.getKey();
        DateTime now = getTime();
        if (start_time >= limit_time) {
          done = 0;
          break;
        }
        if (keypressed != NO_KEY) {
          if (keypressed == 'A') {
            done = 0;
            break;
          }
          else if (keypressed == 'B') {
            done = 1;
            break;
          }
          else if (keypressed == 'C') {
            done = 2;
            break;
          }
          else if (keypressed == 'D') {
            done = 3;
            break;
          }
        }
        else {
          if (limit_time > 48) {
            start_time = now.second();
          }
          else {
            start_time = start_time_second();
          }
        }
      }

      if (done == 3) {
        *state = 6;
      }
      else if (done == 0) {
        *state = 0;
        lcd.clear();
        break;
      }
      else if (done == 1) {
        *state = 1;
        lcd.clear();
        break;
      }
      else if (done == 2) {
        *state = 2;
        lcd.clear();
      }
      else {
        *state = 5;
      }
    }
    else if (*state == 6)
    {
      Serial.print(matkul_tambahan); Serial.print(" "); Serial.println(matkul_tambahan.length());
      Serial.print(kelas_tambahan); Serial.print(" "); Serial.println(kelas_tambahan.length());
      Serial.print(durasi_tambahan); Serial.print(" "); Serial.println(durasi_tambahan.length());
      matkulTambahan(matkul_tambahan.c_str(), kelas_tambahan.c_str(), durasi_tambahan.c_str());
      lcd.clear();
      break;
    }
  }
}


void MatchMode() {

  if (getFinger) {
    uint16_t fingerid = matchFingerprint();
    if (fingerid != nofinger) {
      if (fingerid == notfound) {
        speedBuzzer(50, 2);
        lcd.setCursor(0, 2);
        lcd.print("      Try Again     ");
      }
      else if (fingerid > 0) {
        speedBuzzer(100, 1);

        String read_config, read_absen_all;
        char* read_absen_single;
        bool finger_done = false;

        read_absen_all = readFile(SD, "/absen_done.txt");
        read_absen_single = strtok((char*)read_absen_all.c_str(), ",");

        while (read_absen_single != NULL) {
          if (atoi(read_absen_single) == fingerid) {
            finger_done = true;
            break;
          }
          read_absen_single = strtok(NULL, ",");
        }

        if (finger_done) {
          if (fingerid % 2 == 1) {
            fingerid = fingerid + 1;
          }
          else {
            fingerid = fingerid;
          }

          fingerid = fingerid / 2;

          //Dosen
          if (fingerid <= counter_dosen) {
            lcd.setCursor(0, 1);
            lcd.print(" " + nim[fingerid] + " ");
            lcd.setCursor(0, 2);
            lcd.print("        Done        ");
            delay(500);
            lcd.clear();
            showBatreMatkul();
          }
          //Asisten
          else if (fingerid <= (counter_dosen + counter_asisten)) {
            lcd.setCursor(0, 2);
            lcd.print("   " + nim[fingerid] + " Done    ");
          }
          //Mahasiswa
          else {
            lcd.setCursor(0, 2);
            lcd.print("   " + nim[fingerid] + " Done    ");
          }
        }
        else {
          if (fingerid % 2 == 1) {
            fingerid = fingerid + 1;
          }
          else {
            fingerid = fingerid;
          }
          appendFile(SD, "/absen_done.txt", (String(fingerid) + ",").c_str());
          appendFile(SD, "/absen_done.txt", (String(fingerid - 1) + ",").c_str());

          fingerid = fingerid / 2;

          //Dosen
          if (fingerid <= counter_dosen) {
            lcd.setCursor(0, 1);
            lcd.print("      Welcome       ");
            lcd.setCursor(0, 2);
            lcd.print(" " + nim[fingerid] + " ");
            upload_fingerprint_log(nim[fingerid], nama[fingerid], kode_ruangan, kode_matakuliah, kelas, "Dosen");
            delay(500);
            lcd.clear();
            showBatreMatkul();
          }
          //Asisten
          else if (fingerid <= (counter_dosen + counter_asisten)) {
            lcd.setCursor(0, 2);
            lcd.print("  Welcome " + nim[fingerid] + "  ");
            upload_fingerprint_log(nim[fingerid], nama[fingerid], kode_ruangan, kode_matakuliah, kelas, "Asisten");
          }
          //Mahasiswa
          else {
            lcd.setCursor(0, 2);
            lcd.print("  Welcome " + nim[fingerid] + "  ");
            upload_fingerprint_log(nim[fingerid], nama[fingerid], kode_ruangan, kode_matakuliah, kelas, "Mahasiswa");
          }
        }
      }
    }
    else {
      lcd.setCursor(0, 2);
      lcd.print("Waiting for finger..");
    }
  }
  else {
    lcd.setCursor(0, 2);
    lcd.print("-- No class found --");
  }
}

void fsm(uint8_t *input, uint8_t *state) {
  if (*state == 0)
  {
    MatchMode();
    if (*input == 1) {
      *state = 1;
    }
    else if (*input == 2) {
      *state = 2;
    }
    else {
      *state = 0;
    }
  }
  else if (*state == 1)
  {
    lcd.setCursor(0, 0);
    lcd.print("---   Register   ---");
    delay(2000);
    lcd.setCursor(0, 2);
    lcd.print("Checking Connection ");
    delay(3000);

    if ((Ethernet.linkStatus() == LinkON) || (WiFi.status() == WL_CONNECTED)) {
      lcd.setCursor(0, 3);
      lcd.print("  Device Connected  ");
      delay(2000);
      enrollFingerprint();
      lcd.clear();
      *state = 0;
      *input = 0;
    }
    else {
      lcd.setCursor(0, 3);
      lcd.print("Device Not Connected");
      delay(2000);
      lcd.clear();
      *state = 0;
      *input = 0;
    }
  }
  else
  {
    lcd.setCursor(0, 0);
    lcd.print("---- Add Class  ----");
    delay(2000);
    lcd.setCursor(0, 2);
    lcd.print("Checking Connection ");
    delay(3000);

    if ((Ethernet.linkStatus() == LinkON) || (WiFi.status() == WL_CONNECTED)) {
      lcd.setCursor(0, 3);
      lcd.print("  Device Connected  ");
      delay(2000);
      lcd.clear();
      fsm_kelastambahan(input, state);
      *state = 0;
      *input = 0;
    }
    else {
      lcd.setCursor(0, 3);
      lcd.print("Device Not Connected");
      delay(2000);
      lcd.clear();
      *state = 0;
      *input = 0;
    }
  }
}
