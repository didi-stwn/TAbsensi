import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import get from './config';

class Ikbal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            nama: '',
            x: '',
            y: '',
            z: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitDaftar = this.handleSubmitDaftar.bind(this);
    }
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }
    handleSubmitDaftar(e) {
        e.preventDefault();
        const { nama, x, y, z } = this.state
        fetch(get.createtest, {
            method: 'post',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                nama: nama,
                x: x,
                y: y,
                z: z,
            })
        })
            .then(response => response.json())
            .then(response => {
                setTimeout(this.componentDidMount(), 1000)
            })
            .catch(error => {
                sessionStorage.clear()
                window.location.reload()
            })
    }
    componentDidMount() {
        fetch(get.readtest, {
            method: 'get',
            headers: {
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(response => {
                //berhasil dapet data
                if (response.status === 1) {
                    this.setState({ data: response.pesan })
                }
                //ga dapet token
                else if ((response.status !== 1) && (response.status !== 0)) {
                    sessionStorage.clear()
                    window.location.reload()
                }
            })
            .catch(error => {
                sessionStorage.clear()
                window.location.reload()
            })
    }
    delete(nama) {
        var yes = window.confirm(`Apakah anda yakin ingin menghapus data: ${nama} ?`);
        if (yes === true) {
            fetch(get.deletetest, {
                method: 'delete',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    nama: nama,
                })
            })
                .then(response => response.json())
                .then(response => {
                    setTimeout(this.componentDidMount(), 1000)
                })
                .catch(error => {
                    sessionStorage.clear()
                    window.location.reload()
                })
        }
    }
    render() {
        var i = 0
        const state = this.state
        return (
            <div>
                <div className="kotakfilter2">
                    <form className="kotakforminputlogpintu" onSubmit={this.handleSubmitDaftar}>
                        <div className="kotakinputruangandevice2">
                            <label><b>Nama</b> </label> <br></br>
                            <input name="nama" onChange={this.handleChange} className="inputformlaporannim" type="text" placeholder="nama" required ></input>
                        </div>

                        <div className="kotakinputruanganruangan2">
                            <label><b>Vektor X</b> </label> <br></br>
                            <input name="x" onChange={this.handleChange} className="inputformlaporannim" type="text" placeholder="Vektor x" required ></input>
                        </div>

                        <div className="kotakinputruanganalamat2">
                            <label><b>Vektor Y</b> </label> <br></br>
                            <input name="y" onChange={this.handleChange} className="inputformlaporannim" type="text" placeholder="Vektor y" required ></input>
                        </div>

                        <div className="kotakinputruanganjumlah2">
                            <label><b>Vektor Z</b> </label> <br></br>
                            <input name="z" onChange={this.handleChange} className="inputformlaporannim" type="text" placeholder="Vektor z" required ></input>
                        </div>

                        <div className="kotaksubmitpenggunadaftar">
                            <input className="submitformlogpintu" type="submit" value="Add"></input>
                        </div>
                    </form>
                </div>
                <div className="paddingtop30px2"></div>
                <div className="kotakfilter3">
                    <div style={{ padding: '30px' }}>
                        <div className="isitabel">
                            <table className="tableruangan">
                                <thead className="theadlog">
                                    <tr>
                                        <th className="status" >Nama</th>
                                        <th className="status" >Data X</th>
                                        <th className="status" >Data Y</th>
                                        <th className="status" >Data Z</th>
                                        <th className="keterangan">Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody className="tbodylog">
                                    {state.data.map(isidata => (
                                        <tr key={i++}>
                                            <td>{isidata.nama}</td>
                                            <td>{isidata.x}</td>
                                            <td>{isidata.y}</td>
                                            <td>{isidata.z}</td>
                                            <td>
                                                <div>
                                                    <button className="backgroundmerah" onClick={() => this.delete(isidata.nama)}>Delete</button>
                                                </div>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Ikbal);