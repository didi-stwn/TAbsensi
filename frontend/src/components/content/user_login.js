import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import get from './config';
import hash from 'object-hash';

class UserLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //read
            sortby: 'status',
            ascdsc: 'asc',
            search: '',
            limit: 10,
            page: 1,
            rowCount: 0,
            data: [],
            datakosong: true,
            daftar: false,
            edit: false,

            //create
            usernamec: '',
            password1c: '',
            password2c: '',
            statusc: '',

            //edit
            usernameu: '',
            password1u: '',
            password2u: '',

            //status add
            pesan: '',
            datasalah: false,
            databenar: false,
        };
        this.handleFilter = this.handleFilter.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitDaftar = this.handleSubmitDaftar.bind(this);
        this.handleSubmitEdit = this.handleSubmitEdit.bind(this);
    }

    getData(sortby, ascdsc, search, limit, page) {
        fetch(get.getUserLogin, {
            method: 'post',
            headers: {
                "x-access-token": sessionStorage.name,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                sortby: sortby,
                ascdsc: ascdsc,
                search: search,
                limit: limit,
                page: page,
            })
        })
            .then(response => response.json())
            .then(response => {
                //berhasil dapet data
                if ((response.status === 1) && (response.count !== 0)) {
                    this.setState({ data: response.hasil })
                    this.setState({ rowCount: response.count })
                    this.setState({ datakosong: false })
                }
                else if ((response.status === 1) && (response.count === 0)) {
                    this.setState({ datakosong: true })
                    this.setState({ rowCount: response.count })
                }
                //ga dapet token
                else if ((response.status !== 1) && (response.status !== 0)) {
                    sessionStorage.clear()
                    window.location.reload()
                }
            })
            .catch(error => {
                sessionStorage.clear()
                window.location.reload()
            })
    }

    handleFilter(e) {
        const { sortby, ascdsc, search, limit, page } = this.state
        const { name, value } = e.target;
        this.setState({ [name]: value });
        if (name === "search") {
            var searching = value
            var max = limit
        }
        else if (name === "limit") {
            searching = search
            max = value
        }
        this.getData(sortby, ascdsc, searching, max, page)
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    componentDidMount() {
        const { sortby, ascdsc, search, limit, page } = this.state
        this.getData(sortby, ascdsc, search, limit, page)
    }

    filter(pagenow, sortbynow, ascdscnow) {
        const { sortby, search, limit, page } = this.state
        if (pagenow === page) {
            if (sortbynow === sortby) {
                if (ascdscnow === "asc") {
                    ascdscnow = "desc"
                }
                else if (ascdscnow === "desc") {
                    ascdscnow = "asc"
                }
            }
            else {
                ascdscnow = "asc"
            }
        }
        this.setState({ page: pagenow })
        this.setState({ sortby: sortbynow })
        this.setState({ ascdsc: ascdscnow })
        this.getData(sortbynow, ascdscnow, search, limit, pagenow)
    }

    handleSubmitDaftar(e) {
        e.preventDefault();
        const { usernamec, password1c, password2c, statusc } = this.state;
        if (password1c === password2c) {
            fetch(get.registerUserLogin, {
                method: 'post',
                headers: {
                    "x-access-token": sessionStorage.name,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    username: usernamec,
                    password: hash(password1c),
                    status: statusc,
                })
            })
                .then(response => response.json())
                .then(response => {
                    //berhasil add data
                    if (response.status === 1) {
                        this.setState({ databenar: true })
                        this.setState({ datasalah: false })
                        this.setState({ pesan: response.pesan })
                        setTimeout(this.componentDidMount(), 1000)
                    }
                    //tidak berhasil add data
                    else if (response.status === 0) {
                        this.setState({ databenar: false })
                        this.setState({ datasalah: true })
                        this.setState({ pesan: response.pesan })
                    }
                    //ga ada token
                    else {
                        sessionStorage.clear()
                        window.location.reload()
                    }
                })
                .catch(error => {
                    sessionStorage.clear()
                    window.location.reload()
                })
        }
        else {
            this.setState({ databenar: false })
            this.setState({ datasalah: true })
            this.setState({ pesan: 'Password is incorrect' })
        }
    }

    handleSubmitEdit(e) {
        e.preventDefault();
        const { usernameu, password1u, password2u } = this.state;
        if (password1u === password2u) {
            fetch(get.forgotPassword, {
                method: 'post',
                headers: {
                    "x-access-token": sessionStorage.name,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    username: usernameu,
                    password: hash(password1u),
                })
            })
                .then(response => response.json())
                .then(response => {
                    //berhasil add data
                    if (response.status === 1) {
                        this.setState({ databenar: true })
                        this.setState({ datasalah: false })
                        this.setState({ pesan: response.pesan })
                        setTimeout(this.componentDidMount(), 1000)
                    }
                    //tidak berhasil add data
                    else if (response.status === 0) {
                        this.setState({ databenar: false })
                        this.setState({ datasalah: true })
                        this.setState({ pesan: response.pesan })
                    }
                    //ga ada token
                    else {
                        sessionStorage.clear()
                        window.location.reload()
                    }
                })
                .catch(error => {
                    sessionStorage.clear()
                    window.location.reload()
                })
        }
        else {
            this.setState({ databenar: false })
            this.setState({ datasalah: true })
            this.setState({ pesan: 'Password is incorrect' })
        }
    }

    deletePengguna(a) {
        var yes = window.confirm("Apakah anda yakin ingin menghapus username: " + a + "?");
        if (yes === true) {
            fetch(get.deleteUserLogin, {
                method: 'post',
                headers: {
                    "Authorization": sessionStorage.name,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    username: a,
                })
            })
                .then(response => response.json())
                .then(response => {
                    setTimeout(this.componentDidMount(), 1000)
                })
                .catch(error => {
                    sessionStorage.clear()
                    window.location.reload()
                })
        }
    }

    showDaftar() {
        this.setState({ daftar: true })
        this.setState({ edit: false })
    }
    hideDaftar() {
        this.setState({ daftar: false })
        this.setState({ datasalah: false })
        this.setState({ databenar: false })
    }
    showEdit(a) {
        this.setState({ edit: true })
        this.setState({ daftar: false })
        this.setState({ usernameu: a })
    }
    hideEdit() {
        this.setState({ edit: false })
        this.setState({ datasalah: false })
        this.setState({ databenar: false })
    }
    render() {
        const state = this.state
        //setting tombol berikutnya and sebelumnya
        var maxPage = parseInt(state.rowCount / state.limit);
        if ((state.rowCount % state.limit) !== 0) {
            maxPage = maxPage + 1
        }
        var showNext = false;
        var showPrevious = false;
        // deteksi page pertama
        if (state.page === 1) {
            showPrevious = false;
            if ((state.page === maxPage) || (maxPage === 0)) {
                showNext = false;
            }
            else {
                showNext = true;
            }
        }
        // deteksi page terakhir
        else if ((state.page === maxPage) || (maxPage === 0)) {
            showPrevious = true;
            showNext = false;
        }
        //deteksi page ditengah
        else {
            showPrevious = true;
            showNext = true;
        }

        var aksidata
        if (state.daftar === true) {
            aksidata = "show"
        }
        else if (state.edit === true) {
            aksidata = "show"
        }
        else {
            aksidata = "hide"
        }
        var i = 1;

        return (
            <div>
                {state.daftar &&
                    <div>
                        <div className="kotakfilter2">
                            <form className="kotakforminputlogpintu" onSubmit={this.handleSubmitDaftar}>
                                {
                                    state.databenar &&
                                    <span className="texthijau">{state.pesan}</span>
                                }
                                {
                                    state.datasalah &&
                                    <span className="textmerah">{state.pesan}</span>
                                }

                                <div className="kotakinputruangandevice2">
                                    <label><b>Status</b> </label> <br></br>
                                    <select name="statusc" onChange={this.handleChange} className="inputformlaporannim" placeholder="Status..." required>
                                        <option> </option>
                                        <option key={i++} value={'Administrator'}>Administrator</option>
                                        <option key={i++} value={'Dosen'}>Dosen</option>
                                        <option key={i++} value={'Mahasiswa'}>Mahasiswa</option>
                                    </select>
                                </div>

                                <div className="kotakinputruanganruangan2">
                                    <label><b>Username</b> </label> <br></br>
                                    <input name="usernamec" onChange={this.handleChange} className="inputformlaporannim" type="text" placeholder="Username..." required ></input>
                                </div>

                                <div className="kotakinputruanganalamat2">
                                    <label><b>Password</b> </label> <br></br>
                                    <input name="password1c" onChange={this.handleChange} className="inputformlaporannim" type="password" placeholder="Password..." required ></input>
                                </div>

                                <div className="kotakinputruanganjumlah2">
                                    <label><b>Retry Password</b> </label> <br></br>
                                    <input name="password2c" onChange={this.handleChange} className="inputformlaporannim" type="password" placeholder="Retry Password..." required ></input>
                                </div>

                                <div className="kotaksubmitpenggunadaftar">
                                    <input className="submitformlogpintu" type="submit" value="Add"></input>
                                </div>

                                <div className="kotakcancelpenggunadaftar">
                                    <button className="buttonlikea" onClick={() => this.hideDaftar()}> <span className="cancelformpengguna">Cancel</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                }
                {
                    state.edit &&
                    <div>
                        <div className="kotakfilter2">
                            <form className="kotakforminputlogpintu" onSubmit={this.handleSubmitEdit}>
                                {
                                    state.databenar &&
                                    <span className="texthijau">{state.pesan}</span>
                                }
                                {
                                    state.datasalah &&
                                    <span className="textmerah">{state.pesan}</span>
                                }

                                <div className="inputcreatestatuspenggunamatkul">
                                    <label><b>Username</b> </label> <br></br>
                                    <input onChange={this.handleChange} className="inputcreatepenggunamatkul" type="text" placeholder="Username..." value={state.usernameu || ''} required ></input>
                                </div>

                                <div className="inputcreatenimpenggunamatkul">
                                    <label><b>Password</b> </label> <br></br>
                                    <input name="password1u" onChange={this.handleChange} className="inputcreatepenggunamatkul" type="password" placeholder="Password..." required ></input>
                                </div>

                                <div className="inputcreatenamapenggunamatkul">
                                    <label><b>Retry Password</b> </label> <br></br>
                                    <input name="password2u" onChange={this.handleChange} className="inputcreatepenggunamatkul" type="password" placeholder="Retry Password..." required></input>
                                </div>

                                <div className="kotaksubmitpenggunadaftar">
                                    <input className="submitformlogpintu" type="submit" value="Add"></input>
                                </div>

                                <div className="kotakcancelpenggunadaftar">
                                    <button className="buttonlikea" onClick={() => this.hideEdit()}> <span className="cancelformpengguna">Cancel</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                }
                {(state.daftar === false) && (state.edit === false) &&
                    <div className="kotakdaftarruangan">
                        <button className="buttonlikea" onClick={() => this.showDaftar()}>
                            <div className="daftarfakultas">
                                <i className="fa fa-plus"></i>
                                <span><b>&nbsp;&nbsp;Akun Login</b></span>
                            </div>
                        </button>
                    </div>
                }
                <div id={aksidata} className="kotakdata">
                    <div className="tampilkanpage">
                        <b>Tampilkan&nbsp;&nbsp;</b>
                        <select name="limit" onChange={this.handleFilter} className="inputfilterpagelogpintu" required>
                            <option value={10}> 10 </option>
                            <option value={20}> 20 </option>
                            <option value={30}> 30 </option>
                            <option value={40}> 40 </option>
                            <option value={50}> 50 </option>
                            <option value={100}> 100 </option>
                        </select>
                        <b> &nbsp; dari &nbsp;</b>
                        <b>{state.rowCount}</b>
                        <b>&nbsp;Data</b>
                    </div>
                    <div className="filtersearchlogpintu">
                        <span><b>Search&nbsp;&nbsp;</b></span>
                        <input name="search" onChange={this.handleFilter} className="inputfilterlogpintu" type="text" placeholder="search..." required></input>
                    </div>
                    <div className="marginbottom20px"></div>
                    <div className="isitabel">
                        <table className="tablefakultas">
                            <thead className="theadlog">
                                <tr>
                                    <th className="fakultas" onClick={() => this.filter(state.page, "username", state.ascdsc)}>Username</th>
                                    <th className="jurusan" onClick={() => this.filter(state.page, "status", state.ascdsc)}>Status</th>
                                    <th className="keterangan">Keterangan</th>
                                </tr>
                            </thead>
                            {(state.datakosong === false) &&
                                <tbody className="tbodylog">
                                    {state.data.map(isidata => (
                                        <tr key={i++}>
                                            <td>{isidata.username}</td>
                                            <td>{isidata.status}</td>
                                            <td>
                                                <div>
                                                    <button className="backgroundbiru" onClick={() => this.showEdit(isidata.username)} >
                                                        Edit
                        </button>
                                                    &nbsp;
                        <button className="backgroundmerah" onClick={() => this.deletePengguna(isidata.username)}>
                                                        Delete
                        </button>
                                                </div>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>}
                            {(state.datakosong === true) &&
                                <tbody className="tbodylog">
                                    <tr>
                                        <td colSpan="3">Data tidak ditemukan</td>
                                    </tr>
                                </tbody>}
                        </table>
                    </div>
                    <div className="marginbottom20px"></div>
                    <div className="pagedata">
                        {showPrevious &&
                            <button className="pagesebelumnya" onClick={() => this.filter((state.page - 1), state.sortby, state.ascdsc)}>≪ Sebelumnya</button>
                        }
                        {(showPrevious === false) &&
                            <button className="pagesebelumnyanone">≪ Sebelumnya</button>
                        }
                        {
                            showNext &&
                            <button className="pageberikutnya" onClick={() => this.filter((state.page + 1), state.sortby, state.ascdsc)}>Berikutnya ≫</button>
                        }
                        {
                            (showNext === false) &&
                            <button className="pageberikutnyanone">Berikutnya ≫</button>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(UserLogin);
