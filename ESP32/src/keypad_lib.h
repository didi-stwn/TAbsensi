#include <Keypad.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 20, 4);

#define array_length 7
#define match_mode 0
#define reg_mode 1
#define kelastambahan_mode 2

const byte numRows = 4; //number of rows on the keypad
const byte numCols = 4; //number of columns on the keypad

//keymap defines the key pressed according to the row and columns just as appears on the keypad
char keymap[numRows][numCols] =
{
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

//Code that shows the the keypad connections to the arduino terminals
byte rowPins[numRows] = {27, 14, 12, 13};
byte colPins[numCols] = {32, 33, 25, 26};

//initializes an instance of the Keypad class
Keypad myKeypad = Keypad(makeKeymap(keymap), rowPins, colPins, numRows, numCols);

char output[array_length] = {' ', ' ', ' ', ' ', ' ', ' ', ' '};
byte c1r1, c1r2, c1r3, c2r1, c2r2, c2r3, c3r1, c3r2, c3r3 = 0;
byte position_key = 1;
byte key_mode = 0;
bool no_key_10 = true; //true saat ga ada key yang dipencet buat trigger keypad kalau 10 detik ga dipencet
int8_t cursor_position = 0;
//create char https://maxpromer.github.io/LCD-Character-Creator/
byte customLoading[] = {
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111
};

byte customBaterai20[] = {
  B01110,
  B11011,
  B10001,
  B10001,
  B10001,
  B10001,
  B11111,
  B11111
};

byte customBaterai40[] = {
  B01110,
  B11011,
  B10001,
  B10001,
  B10001,
  B11111,
  B11111,
  B11111
};

byte customBaterai60[] = {
  B01110,
  B11011,
  B10001,
  B10001,
  B11111,
  B11111,
  B11111,
  B11111
};

byte customBaterai80[] = {
  B01110,
  B11011,
  B10001,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111
};

byte customBaterai100[] = {
  B01110,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111
};

byte customCharging[] = {
  B01010,
  B01010,
  B01010,
  B11111,
  B11111,
  B11111,
  B11111,
  B01110
};

char keynumber(void) {
  char output = myKeypad.getKey();
  while (output == NO_KEY) {
    output = myKeypad.getKey();
  }
  return output;
}

void f_c1r1(byte a, byte b) {
  c1r2 = 0;
  c1r3 = 0;
  c2r1 = 0;
  c2r2 = 0;
  c2r3 = 0;
  c3r1 = 0;
  c3r2 = 0;
  c3r3 = 0;
  if (a == 1) {
    output[b] = '1';
  }
  else if (a == 2) {
    output[b] = 'A';
  }
  else if (a == 3) {
    output[b] = 'B';
  }
  else if (a == 4) {
    output[b] = 'C';
  }
}
void f_c1r2(byte a, byte b) {
  c1r1 = 0;
  c1r3 = 0;
  c2r1 = 0;
  c2r2 = 0;
  c2r3 = 0;
  c3r1 = 0;
  c3r2 = 0;
  c3r3 = 0;
  if (a == 1) {
    output[b] = '2';
  }
  else if (a == 2) {
    output[b] = 'D';
  }
  else if (a == 3) {
    output[b] = 'E';
  }
  else if (a == 4) {
    output[b] = 'F';
  }
}
void f_c1r3(byte a, byte b) {
  c1r2 = 0;
  c1r1 = 0;
  c2r1 = 0;
  c2r2 = 0;
  c2r3 = 0;
  c3r1 = 0;
  c3r2 = 0;
  c3r3 = 0;
  if (a == 1) {
    output[b] = '3';
  }
  else if (a == 2) {
    output[b] = 'G';
  }
  else if (a == 3) {
    output[b] = 'H';
  }
  else if (a == 4) {
    output[b] = 'I';
  }
}
void f_c2r1(byte a, byte b) {
  c1r2 = 0;
  c1r3 = 0;
  c1r1 = 0;
  c2r2 = 0;
  c2r3 = 0;
  c3r1 = 0;
  c3r2 = 0;
  c3r3 = 0;
  if (a == 1) {
    output[b] = '4';
  }
  else if (a == 2) {
    output[b] = 'J';
  }
  else if (a == 3) {
    output[b] = 'K';
  }
  else if (a == 4) {
    output[b] = 'L';
  }
}
void f_c2r2(byte a, byte b) {
  c1r2 = 0;
  c1r3 = 0;
  c2r1 = 0;
  c1r1 = 0;
  c2r3 = 0;
  c3r1 = 0;
  c3r2 = 0;
  c3r3 = 0;
  if (a == 1) {
    output[b] = '5';
  }
  else if (a == 2) {
    output[b] = 'M';
  }
  else if (a == 3) {
    output[b] = 'N';
  }
  else if (a == 4) {
    output[b] = 'O';
  }
}
void f_c2r3(byte a, byte b) {
  c1r2 = 0;
  c1r3 = 0;
  c2r1 = 0;
  c2r2 = 0;
  c1r1 = 0;
  c3r1 = 0;
  c3r2 = 0;
  c3r3 = 0;
  if (a == 1) {
    output[b] = '6';
  }
  else if (a == 2) {
    output[b] = 'P';
  }
  else if (a == 3) {
    output[b] = 'Q';
  }
  else if (a == 4) {
    output[b] = 'R';
  }
}
void f_c3r1(byte a, byte b) {
  c1r2 = 0;
  c1r3 = 0;
  c2r1 = 0;
  c2r2 = 0;
  c2r3 = 0;
  c1r1 = 0;
  c3r2 = 0;
  c3r3 = 0;
  if (a == 1) {
    output[b] = '7';
  }
  else if (a == 2) {
    output[b] = 'S';
  }
  else if (a == 3) {
    output[b] = 'T';
  }
  else if (a == 4) {
    output[b] = 'U';
  }
}
void f_c3r2(byte a, byte b) {
  c1r2 = 0;
  c1r3 = 0;
  c2r1 = 0;
  c2r2 = 0;
  c2r3 = 0;
  c3r1 = 0;
  c1r1 = 0;
  c3r3 = 0;
  if (a == 1) {
    output[b] = '8';
  }
  else if (a == 2) {
    output[b] = 'V';
  }
  else if (a == 3) {
    output[b] = 'W';
  }
}
void f_c3r3(byte a, byte b) {
  c1r2 = 0;
  c1r3 = 0;
  c2r1 = 0;
  c2r2 = 0;
  c2r3 = 0;
  c3r1 = 0;
  c3r2 = 0;
  c1r1 = 0;
  if (a == 1) {
    output[b] = '9';
  }
  else if (a == 2) {
    output[b] = 'X';
  }
  else if (a == 3) {
    output[b] = 'Y';
  }
  else if (a == 4) {
    output[b] = 'Z';
  }
}

void keypad_init() {
  position_key = 1;
  //lcd.clear();
  cursor_position = -1;
  for (byte k = 1; k < array_length; k++) {
    output[k] = ' ';
  }
  c1r1 = 0;
  c1r2 = 0;
  c1r3 = 0;
  c2r1 = 0;
  c2r2 = 0;
  c2r3 = 0;
  c3r1 = 0;
  c3r2 = 0;
  c3r3 = 0;
}

byte start_time_second() {
  byte output;
  DateTime now = getTime();
  output = now.second();

  if (output >= 49) {
    output = output - 49;
  }
  else {
    output = output;
  }

  return output;
}

void waitForKeypad(String *str_out, uint8_t *key) {
  String output_str = "\0";
  char keypressed = myKeypad.getKey();
  lcd.setCursor(0, 2);
  keypad_init();

  //untuk mentrigger jika tombol ga dipencet selama 10 detik akan keluar loop
  byte initial_time, limit_time;

  initial_time = start_time_second();
  limit_time = initial_time + 10;

  no_key_10 = false;

  while ((keypressed != 'A') && (keypressed != 'B') && (keypressed != 'C') && (keypressed != 'D')) {
    if (keypressed != NO_KEY)
    {
      initial_time = start_time_second();
      limit_time = initial_time + 10;

      if (keypressed == '1') {
        c1r1 = c1r1 + 1;
        if (c1r1 > 4) {
          c1r1 = 1;
        }
        f_c1r1(c1r1, position_key);
        cursor_position = -1;
        //Serial.println(output);
      }
      else if (keypressed == '2') {
        c1r2 = c1r2 + 1;
        if (c1r2 > 4) {
          c1r2 = 1;
        }
        f_c1r2(c1r2, position_key);
        cursor_position = -1;
        //Serial.println(output);
      }
      else if (keypressed == '3') {
        c1r3 = c1r3 + 1;
        if (c1r3 > 4) {
          c1r3 = 1;
        }
        f_c1r3(c1r3, position_key);
        cursor_position = -1;
        //Serial.println(output);
      }
      else if (keypressed == '4') {
        c2r1 = c2r1 + 1;
        if (c2r1 > 4) {
          c2r1 = 1;
        }
        f_c2r1(c2r1, position_key);
        cursor_position = -1;
        //Serial.println(output);
      }
      else if (keypressed == '5') {
        c2r2 = c2r2 + 1;
        if (c2r2 > 4) {
          c2r2 = 1;
        }
        f_c2r2(c2r2, position_key);
        cursor_position = -1;
        //Serial.println(output);
      }
      else if (keypressed == '6') {
        c2r3 = c2r3 + 1;
        if (c2r3 > 4) {
          c2r3 = 1;
        }
        f_c2r3(c2r3, position_key);
        cursor_position = -1;
        //Serial.println(output);
      }
      else if (keypressed == '7') {
        c3r1 = c3r1 + 1;
        if (c3r1 > 4) {
          c3r1 = 1;
        }
        f_c3r1(c3r1, position_key);
        cursor_position = -1;
        //Serial.println(output);
      }
      else if (keypressed == '8') {
        c3r2 = c3r2 + 1;
        if (c3r2 > 3) {
          c3r2 = 1;
        }
        f_c3r2(c3r2, position_key);
        cursor_position = -1;
        //Serial.println(output);
      }
      else if (keypressed == '9') {
        c3r3 = c3r3 + 1;
        if (c3r3 > 4) {
          c3r3 = 1;
        }
        f_c3r3(c3r3, position_key);
        cursor_position = -1;
        //Serial.println(output);
      }
      else if (keypressed == '0') {
        c1r1 = 0;
        c1r2 = 0;
        c1r3 = 0;
        c2r1 = 0;
        c2r2 = 0;
        c2r3 = 0;
        c3r1 = 0;
        c3r2 = 0;
        c3r3 = 0;
        output[position_key] = '0';
        cursor_position = -1;
      }
      else if (keypressed == '#') {
        cursor_position = position_key;
        position_key = position_key + 1;
        if (position_key > (array_length - 1)) {
          position_key = 0;
        }
        c1r1 = 0;
        c1r2 = 0;
        c1r3 = 0;
        c2r1 = 0;
        c2r2 = 0;
        c2r3 = 0;
        c3r1 = 0;
        c3r2 = 0;
        c3r3 = 0;
      }
      else if (keypressed == '*') {
        keypad_init();
      }
    }
    else {
      DateTime now = getTime();
      if (initial_time >= limit_time) {
        keypressed = 'E';
        no_key_10 = true;
        break;
      }
      if (limit_time > 48) {
        initial_time = now.second();
      }
      else {
        initial_time = start_time_second();
      }

      if (cursor_position >= 0)
      {
        lcd.setCursor(cursor_position, 2);
        lcd.cursor();
      }
      else {
        lcd.noCursor();
        for (byte k = 1; k < array_length; k++) {
          lcd.setCursor(k - 1, 2);
          lcd.print(output[k]);
        }
      }
    }
    keypressed = myKeypad.getKey();
  }

  if (keypressed == 'D') {
    no_key_10 = false;
    output_str = "\0";
    for (byte k = 1; k < array_length; k++) {
      if (output[k] != ' ') {
        output_str += String(output[k]);
      }
    }
    *str_out = output_str;
    for (byte k = 1; k < array_length; k++) {
      output[k] = ' ';
    }
    *key = 3;
  }
  else if (keypressed == 'A') {
    no_key_10 = false;
    *key = 0;
  }
  else if (keypressed == 'B') {
    no_key_10 = false;
    *key = 1;
  }
  else if (keypressed == 'C') {
    no_key_10 = false;
    *key = 2;
  }
  else {
    *key = 4;
  }
}

byte keypadMode(char key) {
  byte n;

  if (key == 'A') {
    n = 0;
  }
  else if (key == 'B') {
    n = 1;
  }
  else if (key == 'C') {
    n = 2;
  }
  else if (key == 'D') {
    n = 3;
  }

  return n;
}
